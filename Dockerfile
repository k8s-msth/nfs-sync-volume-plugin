FROM alpine

RUN apk add --no-cache --virtual .run-deps rsync jq && rm -rf /var/cache/apk/*

WORKDIR /usr/src/app

ADD dummy /usr/src/app/dummy
ADD entrypoint.sh /usr/src/app/entrypoint.sh
ADD nfs-sync /usr/src/app/nfs-sync
