# nfs-sync-volume-plugin

Hacky implementation of a Kubernetes Flexvolume plugin to sync an NFS path to a hostpath dir.


  nodeSelector:
    k8s-msth/nfs-sync: "true"

## Requirements

- nfs client
- rsync


## k3s

Path where the volume plugin lives : 

/usr/libexec/kubernetes/kubelet-plugins/volume/exec

The volume plugin will work with any reload or restart of k3s


  volumes:
  - name: sync
    flexVolume:
      driver: k8s-msth/dummy
      fsType: "ext4"
      options:
        volumeID: "vol1"
        size: "1000m"
        volumegroup: "kube_vg"


For this example the full path of the driver would be /usr/libexec/kubernetes/kubelet-plugins/volume/exec/k8s-msth~dummy/dummy

### Log

Mon Jun  8 19:17:59 BST 2020 init init
Mon Jun  8 19:17:59 BST 2020 domount /var/lib/kubelet/pods/00479bdb-9665-469d-944a-a4d1d0fadd96/volumes/k8s-msth~dummy/sync {"kubernetes.io/fsType":"ext4","kubernetes.io/pod.name":"nginx","kubernetes.io/pod.namespace":"default","kubernetes.io/pod.uid":"00479bdb-9665-469d-944a-a4d1d0fadd96","kubernetes.io/pvOrVolumeName":"sync","kubernetes.io/readwrite":"rw","kubernetes.io/serviceAccount.name":"default","size":"1000m","volumeID":"vol1","volumegroup":"kube_vg"}
Mon Jun  8 19:27:31 BST 2020 unmount /var/lib/kubelet/pods/00479bdb-9665-469d-944a-a4d1d0fadd96/volumes/k8s-msth~dummy/sync
